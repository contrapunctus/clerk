(defsystem     "clerk"
  :version     "0.0.1"
  :serial      t
  :license     "Unlicense"
  :author      "contrapunctus <contrapunctus at disroot dot org>"
  :maintainer  "contrapunctus <contrapunctus at disroot dot org>"
  :description "File manager for CLIM"
  :depends-on  ("mcclim" "bordeaux-threads" "rutils" "clim-app-base" "anathema"
                         (:feature (:not :closos) "file-notify"))
  :components  ((:module "src"
                 :serial t
                 :components ((:file "package")
                              (:file "core")
                              (:file "styles")
                              (:file "view-dired")
                              (:file "view-tree")
                              (:file "frame-and-commands")
                              (:file "navigation")
                              (:file "mark")))))
