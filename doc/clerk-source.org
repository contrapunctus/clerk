# -*- org-confirm-babel-evaluate: nil; -*-
#+TITLE: clerk
#+EXPORT_FILE_NAME: clerk.org
#+OPTIONS: prop:t toc:2

# This file is meant to be exported to clerk.org in order to generate
# the reference documentation. The code to generate the reference
# documentation is written in Common Lisp, and thus exporting this
# file requires a Lisp implementation, SLIME, and Quicklisp. It also
# requires that Common Lisp evaluation in Org be enabled.

* Explanation
:PROPERTIES:
:CREATED:  2022-09-16T21:41:25+0530
:END:
Clerk is a file manager made using CLIM. It uses [[https://codeberg.org/contrapunctus/anathema][Anathema]] for theming.

It is meant to serve at least two purposes -

1. A file manager, capable of everything you expect from existing file managers (Thunar, Dired, etc).

2. A file picker widget for other CLIM applications.

* Reference
:PROPERTIES:
:CREATED:  2022-10-15T21:46:07+0530
:CUSTOM_ID: reference
:header-args: :session clerk-doc
:END:
#+BEGIN_SRC lisp :exports none
(ql:quickload '(clerk org-cl-ref))
#+END_SRC

#+BEGIN_SRC lisp :exports results :results value verbatim raw
(org-cl-ref:format-definitions
 '(clerk:mark-mixin
   clerk:cursor-mixin
   clerk:file
   clerk:make-file
   (clerk:directory :type definitions:class)
   clerk:make-directory
   clerk:current
   clerk:up
   clerk:pane
   (clerk:directory :type definitions:class)
   clerk:contents
   clerk:fresh-contents
   clerk:view
   clerk:display-pane-with-view)
 3 "reference-")
#+END_SRC

** Cursor protocol
:PROPERTIES:
:CREATED:  2022-10-15T21:54:20+0530
:CUSTOM_ID: reference-cursor
:END:
#+BEGIN_SRC lisp :exports results :results value verbatim raw
(org-cl-ref:format-definitions
 '(clerk:previous
   clerk:next)
 3 "reference-cursor-")
#+END_SRC

** User commands
:PROPERTIES:
:CREATED:  2022-10-16T07:47:24+0530
:END:
#+BEGIN_SRC lisp :exports results :results value verbatim raw
(org-cl-ref:format-definitions
 '(clerk::com-visit
   clerk::com-visit-cursor
   clerk::com-previous
   clerk::com-next
   clerk::com-parent
   clerk::com-mark
   clerk::com-mark-cursor
   clerk::com-toggle-mark
   clerk::com-unmark
   clerk::com-unmark-cursor
   clerk::com-unmark-all
   clerk::com-invert-marks)
 3 "reference-command-")
#+END_SRC
