;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((org-mode . ((eval . (add-hook 'org-insert-heading-hook
                                (lambda ()
                                  (save-excursion
                                    (org-set-property "CREATED"
                                                      (format-time-string "%FT%T%z"))))
                                nil t)))))
