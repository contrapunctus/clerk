(in-package :clerk)

(defclass mark-mixin ()
  ((%mark :initform nil
          :initarg :mark
          :accessor mark
          :documentation
          "Non-nil if object has been marked by the user for an operation."))
  (:documentation "Mixin for objects which can be marked."))

(defclass cursor-mixin ()
  ((%cursor :initarg :cursor
            :accessor cursor
            :initform nil))
  (:documentation "Mixin for objects which can be navigated using a cursor."))

(defclass file (cab:file mark-mixin cursor-mixin) ())
(defclass directory (cab:directory file) ())
(defclass current (directory) ())
(defclass up (directory) ())

(defmethod print-object ((file mark-mixin) stream)
  (print-unreadable-object (file stream :type t)
    (format stream
            "path: ~A  mark: ~A"
            (cab:absolute-pathname file) (mark file))))

(defun make-file (path)
  (make-instance 'file :absolute-pathname path))

(defun make-directory (path)
  (make-instance 'directory :absolute-pathname path))

(defclass pane (application-pane)
  ((%directory :initarg :directory
               :accessor directory
               :initform (make-directory *default-pathname-defaults*)
               :documentation
               "Instance of `directory' this pane is displaying.")
   (%contents
    :initarg :contents
    :accessor contents
    :initform '()
    :documentation
    "List of objects representing the contents of `%directory', based on `%view'.")
   (%view :initarg :view
          :accessor view
          :initform (make-instance 'clerk.dired:view))
   (%watch-thread :initarg :watch-thread
                  :accessor watch-thread
                  :initform nil
                  :documentation
                  "Instance of `bt:thread' used to process filesystem events."))
  (:default-initargs
   :display-time :command-loop
   :display-function 'display-pane
   :incremental-redisplay t
   :foreground (at:style-foreground-ink ats:*default*)
   :background (at:style-background-ink ats:*default*)))

(defgeneric fresh-contents (pane view)
  (:documentation
   "Return a fresh value for the contents of PANE while accounting for VIEW."))

(defmethod (setf directory) ((new-value directory) (pane pane))
  ;; kill old watch thread and start a new one
  #-closos
  (let* ((frame        *application-frame*)
         (old-dir      (directory pane))
         (old-pathname (cab:absolute-pathname old-dir))
         (new-pathname (cab:absolute-pathname new-value)))
    (when (member old-pathname (fn:list-watched) :test #'equal)
      (fn:unwatch old-pathname))
    (fn:watch new-pathname :events '(:create :delete :move))
    (unless (watch-thread pane)
      (setf (watch-thread pane)
            (bt:make-thread
             (lambda ()
               (fn:with-events (file change :timeout t)
                 (unwind-protect nil
                   (execute-frame-command frame '(com-refresh))))))))
    (setf (slot-value pane '%directory)  new-value
          (contents pane)  (fresh-contents pane (view pane)))))

(defmethod initialize-instance :after ((pane pane) &rest initargs &key &allow-other-keys)
  (setf (directory pane) (directory pane)))

(defgeneric display-pane-with-view (frame pane view))

(defgeneric visit-object (pane view object))

(defgeneric previous (pane view)
  (:documentation "Move cursor to the previous object."))

(defgeneric next (pane view)
  (:documentation "Move cursor to the next object."))

(defun display-pane (frame pane)
  (display-pane-with-view frame pane (view pane)))
