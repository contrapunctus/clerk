(in-package :clerk)

(defclass tabular-view (view) ()
  (:documentation "Display files as a table."))
