(in-package :clerk.dired)

(defclass view (clim:view) ()
  (:documentation "Display files similar to Dired, or ls -l."))

(defun mark-or-indent (file)
  (if (c:mark file)
      " * "
      "   "))

(defun indent (pane)
  (format pane "   "))

(defun enough-pathname (pane maybe-subpath)
  (u:enough-pathname (cab:absolute-pathname maybe-subpath)
                     (cab:absolute-pathname (c:directory pane))))

(define-presentation-method present :around
  (file (type c:cursor-mixin) (pane c:pane) (view view) &key)
  (cond ((cursor file)
         (surrounding-output-with-border (pane :move-cursor nil :padding 2)
           (call-next-method)))
        (t (call-next-method))))

(define-presentation-method present
    (file (type c:file) (pane c:pane) (view view) &key)
  (format pane "~A~%" (enough-pathname pane file)))

(define-presentation-method present
    (directory (type c:directory) (pane c:pane) (view view) &key)
  (at:with-style (pane cs:*directory*)
    (format pane "~A~%" (enough-pathname pane directory))))

(define-presentation-method present
    (directory (type c:current) (pane c:pane) (view view) &key)
  (at:with-style (pane cs:*current-directory*)
    (format pane "~A~%" (cab:absolute-pathname directory))))

(define-presentation-method present
    (directory (type c:up) (pane c:pane) (view view) &key)
  (let ((pane-pathname (cab:absolute-pathname (c:directory pane))))
    ;; Display Up button, unless we've reached the root directory
    (unless (equal pane-pathname (u:pathname-root pane-pathname))
      (format pane "↩~%"))))

(defmethod c:fresh-contents ((pane c:pane) (view view))
  (let* ((directory (cab:absolute-pathname (c:directory pane)))
         (current   (make-instance
                     'c:current
                     :absolute-pathname (cab:absolute-pathname (c:directory pane))))
         (parent    (make-instance
                     'c:up
                     :absolute-pathname
                     (u:pathname-parent-directory-pathname directory)))
         (files     (mapcar #'c:make-file (u:directory-files directory)))
         (subdirs   (mapcar #'c:make-directory (u:subdirectories directory))))
    (cond (subdirs (setf (cursor (first subdirs)) t))
          (files   (setf (cursor (first files)) t))
          (parent  (setf (cursor parent) t))
          (t       (setf (cursor directory) t)))
    (apply #'list current parent (append subdirs files))))

(defmethod clerk:display-pane-with-view (frame (pane clerk:pane) (view view))
  (let ((pane-pathname (cab:absolute-pathname (c:directory pane))))
    ;; Necessary on first iteration
    (unless (c:contents pane)
      (setf (c:directory pane) (c:directory pane)))
    ;; Display files and subdirectories
    (loop :for file :in (c:contents pane)
          :do (format pane (mark-or-indent file))
              (present file (presentation-type-of file)
                       :stream pane :view view))))

(defmethod c:previous ((pane c:pane) (view view))
  (loop :with previous
        :for file :in (c:contents pane)
        :for i :from 0
        :when (and (c:cursor file) (> i 0))
          :return (setf (c:cursor file) nil
                        (c:cursor previous) t)
        :else :do (setf previous file)))

(defmethod c:next ((pane c:pane) (view view))
  (loop :with length = (length (c:contents pane))
        :with previous
        :for file :in (c:contents pane)
        :for i :from 1
        :when (and previous
                   (c:cursor previous)
                   (not (> i length)))
          :return (setf (c:cursor previous) nil
                        (c:cursor file) t)
        :else :do (setf previous file)))
