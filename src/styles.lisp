(in-package :clerk.styles)

(at:define-style directory (ats:highlight) ())
(at:define-style file (ats:default) ())
(at:define-style symlink (ats:method) ())

(at:define-style cursor (ats:default) ())
(at:define-style current-directory (ats:link) ())
