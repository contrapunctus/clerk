(in-package :clerk.tree)

(defclass view (clim:view) ()
  (:documentation "Display files as a tree with collapsible nodes."))

(defclass fold-mixin ()
  ((%fold-state :initarg :fold-state
                :accessor fold-state)))

(defclass file (clerk:file fold-mixin) ())

(defun make-file (path)
  (make-instance 'file :absolute-pathname path))

(defclass directory (clerk:directory fold-mixin) ())

(defun make-directory (path)
  (make-instance 'directory :absolute-pathname path))

(defmethod contents
    ((pane clerk:pane) (view clerk.tree:view))
  (let* ((dir          (clerk:directory pane))
         (dir-pathname (cab:absolute-pathname dir))
         (files        (mapcar #'make-file
                               (u:directory-files dir-pathname)))
         (subdirs      (mapcar #'make-directory
                               (u:subdirectories dir-pathname))))
    (values subdirs files)))

(defmethod clerk:display-pane-with-view
    (frame (pane clerk:pane) (view clerk.tree:view))
  (bind ((dir           (clerk:directory pane))
         (dir-pathname  (cab:absolute-pathname dir))
         (parent        (make-directory
                         (u:pathname-parent-directory-pathname dir-pathname)))
         (subdirs files (contents pane view)))
    ;; (format *debug-io* "display-files: subdirs -~%~a~%files -~%~a~%" subdir-paths files)
    ;; Display Up button
    (unless (equal dir-pathname (u:pathname-root dir-pathname))
      (with-output-as-presentation (pane parent 'directory)
        (format pane "↩~%")))
    ;; Display subdirectories
    (loop for subdir in subdirs
          for path = (cab:absolute-pathname subdir)
          do (with-output-as-presentation (pane subdir 'directory)
               (at:with-style (pane cs:*directory*)
                 (format pane "~A~%" (u:enough-pathname path dir-pathname)))))
    ;; Display files
    (loop for file in files
          for path = (cab:absolute-pathname file)
          do (with-output-as-presentation (pane file 'file)
               (format pane "~A~%" (u:enough-pathname path dir-pathname))))))
