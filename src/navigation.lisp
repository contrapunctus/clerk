(in-package :clerk)

(define-clerk-command (com-visit :name t :menu t) ((directory 'directory))
  (setf (directory (clerk-pane *application-frame*)) directory))

(define-clerk-command (com-visit-cursor :keystroke #\return) ()
  (let* ((pane        (clerk-pane *application-frame*))
         (cursor-file (find-if #'cursor (contents pane))))
    (when (typep cursor-file 'directory)
      (setf (directory pane) cursor-file))))

(define-presentation-to-command-translator click-to-visit
    (directory com-visit clerk) (directory) (list directory))

(define-clerk-command (com-previous :name t) ()
  (let ((pane (clerk-pane *application-frame*)))
    (previous pane (view pane))))

(define-clerk-command (com-next :name t) ()
  (let ((pane (clerk-pane *application-frame*)))
    (next pane (view pane))))

(define-gesture-name :previous :keyboard (#\t))
(define-gesture-name :next :keyboard (#\n))

(add-keystroke-to-command-table 'clerk :previous :command 'com-previous)
(add-keystroke-to-command-table 'clerk :next :command 'com-next)
(add-keystroke-to-command-table 'clerk :select :command 'com-visit-cursor)

(define-clerk-command (com-parent :name t :menu t :keystroke #\b) ()
  (let ((pane (clerk-pane *application-frame*)))
    (setf (directory pane)
          (make-directory
           (u:pathname-parent-directory-pathname (cab:absolute-pathname (directory pane)))))))
