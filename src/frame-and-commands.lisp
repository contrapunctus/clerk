(in-package :clerk)

(define-application-frame clerk ()
  ((%clerk-pane :initarg :clerk-pane
                :accessor clerk-pane))
  (:pointer-documentation t)
  (:panes
   (file-manager
    (multiple-value-bind (pane stream)
        (make-clim-stream-pane :type 'clerk:pane)
      (setf (clerk-pane *application-frame*) stream)
      pane))
   (int :interactor
        :width (/ (graft-width (find-graft)) 2)
        :foreground (at:style-foreground-ink ats:*default*)
        :background (at:style-background-ink ats:*default*)))
  (:layouts (default ;; (vertically () file-manager int)
                     file-manager)))

(define-clerk-command (com-refresh) ()
  (let ((pane (clerk-pane *application-frame*)))
    (setf (directory pane) (directory pane))))

(define-clerk-command (com-move-marked :name t :menu t) ()
  )
(define-clerk-command (com-copy-marked :name t :menu t) ()
  )
(define-clerk-command (com-trash-marked :name t :menu t) ()
  )
(define-clerk-command (com-delete-marked :name t :menu t) ()
  )

(define-clerk-command (com-inspect :name t :menu t) ((file mark-mixin))
  (format *debug-io* "~A~%" file))

(define-clerk-command (com-quit :name t :menu t) ()
  (frame-exit *application-frame*))

(defun run ()
  (find-application-frame 'clerk))
