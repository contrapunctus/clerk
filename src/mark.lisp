(in-package :clerk)

(define-clerk-command (com-mark :name t :menu t) ((file 'mark-mixin))
  (setf (mark file) t))

(define-clerk-command (com-mark-cursor :keystroke #\m) ()
  (let* ((pane        (clerk-pane *application-frame*))
         (cursor-file (find-if #'cursor (contents pane))))
    (com-mark cursor-file)
    (com-next)))

(define-clerk-command (com-toggle-mark) ((file 'mark-mixin))
  (setf (mark file) (not (mark file))))

;; TODO - use some other gesture (I prefer to bind left click to visit)
#+(or)
(define-presentation-to-command-translator click-to-toggle-mark
    (mark-mixin com-toggle-mark clerk)
    (file)
  (list file))

(define-clerk-command (com-unmark :name t :menu t) ((file 'mark-mixin))
  (setf (mark file) nil))

(define-clerk-command (com-unmark-cursor :keystroke #\backspace) ()
  (flet ((cursor-file (pane) (find-if #'cursor (contents pane))))
    (let* ((pane         (clerk-pane *application-frame*))
           (contents     (contents pane))
           (cursor-file  (cursor-file pane))
           (cursor-index (loop :for file :in contents
                               :for i :from 1
                               :when (cursor file) :return i)))
      (cond ((and (mark cursor-file)
                  (= cursor-index (length contents)))
             (com-unmark cursor-file))
            (t (com-previous)
               (com-unmark (cursor-file pane)))))))

(define-clerk-command (com-unmark-all :name t :menu t :keystroke #\U) ()
  (loop :with pane = (clerk-pane *application-frame*)
        :for file :in (contents pane)
        :do (setf (mark file) nil)))

(define-clerk-command (com-invert-marks :name t :menu t :keystroke #\i) ()
  (loop :with pane = (clerk-pane *application-frame*)
        :for file :in (contents pane)
        :for mark = (mark file)
        :do (cond ((eq t mark)
                   (setf (mark file) nil))
                  ((eq nil mark)
                   (setf (mark file) t)))))
