(in-package :cl)

(defpackage :clerk
  (:use :clim :clim-lisp)
  (:shadow #:directory #:pane)
  (:local-nicknames (:at :anathema)
                    (:ats :anathema.style)
                    (:u :uiop)
                    (:cab :clim-app-base)
                    #-closos
                    (:fn :org.shirakumo.file-notify))
  (:export #:mark-mixin #:mark
           #:cursor-mixin #:cursor
           #:file #:directory #:current #:up
           #:make-file #:make-directory
           #:pane #:directory #:contents #:view
           #:fresh-contents
           #:display-pane-with-view
           #:previous #:next
           #:directory-style #:*directory-style*
           #:file #:run))

(defpackage :clerk.styles
  (:local-nicknames (:at :anathema)
                    (:ats :anathema.style))
  (:export #:directory #:current-directory #:file #:symlink #:cursor)
  (:export #:*directory* #:*current-directory* #:*file* #:*symlink* #:*cursor*))

(defpackage :clerk.tree
  (:use :clim :clim-lisp)
  (:shadow #:directory #:view)
  (:import-from :rutils #:bind)
  (:local-nicknames (:at :anathema)
                    (:ats :anathema.style)
                    (:c :clerk)
                    (:cs :clerk.styles)
                    (:u :uiop)
                    (:cab :clim-app-base))
  (:export #:view))

(defpackage :clerk.dired
  (:use :clim :clim-lisp)
  (:shadow #:directory #:view)
  (:local-nicknames (:at :anathema)
                    (:ats :anathema.style)
                    (:c :clerk)
                    (:cs :clerk.styles)
                    (:u :uiop)
                    (:cab :clim-app-base))
  (:export #:view))
