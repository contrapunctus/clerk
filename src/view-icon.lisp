(in-package :clerk)

(defclass icon-view (view)
  ((%icon-size :initarg :icon-size)))
